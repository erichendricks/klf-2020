- Introductions

Tyler, Joel, Cynthia, and Eric 

- Cutover Plan

Tyler and Eric definitely have the contacts to maintain the conference

The more changes we make at one time, the less chance for success we will have

The old planning team will not have a ton of time to help the planning process

KLF has been partnered with a maker space in the past, that partnership has now ended.

- Scheduling

The director of computer science at Wichita State expressed interest in sponsoring the conference next year.

Either a few before or a few weeks after the start of term would work best

- Action Items

Contact Maker Space about any funds remaining in the partnership (Joel)

Investigate getting KLF non-profit status on its own

Reach out to attendees to get media/photos from the event

Talk to Nathan to get webserver access

Schedule Interest Meeting December 4th